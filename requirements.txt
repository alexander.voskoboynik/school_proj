wheel
Django==3.1.7
django-debug-toolbar==3.2
django-extensions
django-forms-bootstrap
MySQL
mysql-connector-python
mysqlclient
celery==5.0.5
django-rest-framework
pytz
lxml
python-memcached
gunicorn
pip install django-bootstrap-pagination
