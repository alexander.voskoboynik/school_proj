$(document).ready(function () {

    // -------------------------------------------------------------------------------------
    // func -> show form
    let showForm = function () {
        const body = $('body');
        const shadow = $('<div class="shadow"></div>');
        $('.super-modal').show();
        body.append(shadow);
    };

    // -------------------------------------------------------------------------------------
    // add student:
    $('#add-student').click(function() {
    showForm();
    $('#student-submit-form').trigger('reset');
    $('.input').val('');
    $('.select').val('');
    $('#student-submit').attr('id', 'student-submit-add')
    });

    $('body').on('click', '#student-submit-add', function (event) {
        event.preventDefault();
        let form = $(this).closest('form');
        let dataToSend = {};
        form.serializeArray().map(function(x){dataToSend[x.name] = x.value;});
        $.post('add/', dataToSend, function (data) {
                if($('.error-msg', $(data)).length) {
                    $('.super-modal').html(data);
                } else {
                    $('#data-students').html(data);
                    $('.shadow').trigger('click');
                }
            });
        });

    // -------------------------------------------------------------------------------------
    // update student:
    let st_id = null;

    $('table').on('click', '.update', function () {
        showForm();
        $('#student-submit').attr('id', 'student-submit-edit')
        let student = $(this).closest('.students').attr('id').split('-');
        // console.log(student);
        st_id = student[0];
        // console.log(st_id);
        let name = student[1];
        let age = student[2];
        let email = student[3];
        $('#id_name').val(name);
        $('#id_age').val(age);
        $('#id_email').val(email);
    });

    $('body').on('click', '#student-submit-edit', function (event) {
        event.preventDefault();
        let form = $(this).closest('form');
        let dataToSend = {};
        let student = st_id
        form.serializeArray().map(function(x){dataToSend[x.name] = x.value;});
        $.post('update/' + student + '/', dataToSend, function (data) {
            if($('.error-msg', $(data)).length) {
                $('.super-modal').html(data);
            } else {
                $('#data-students').html(data);
                $('.shadow').trigger('click');
            }
        });
    });

    //-----------------------------------------------------------------------------------
    // Delete student

    $('table').on('click', '.delete', function () {
        const body = $('body');
        const shadow = $('<div class="shadow"></div>');
        $('.modal-delete').show();
        body.append(shadow);
        $('#student-submit').attr('id', 'student-submit-edit')
        let student = $(this).closest('.students').attr('id').split('-');
        // console.log(student);
        st_id = student[0];
    });

    $('body').on('click', '#student-delete', function (event) {
        event.preventDefault();
        let form = $(this).closest('#student-submit-delete');
        let dataToSend = {};
        let student = st_id;
        form.serializeArray().map(function(x){dataToSend[x.name] = x.value;});
        $.post('delete/' + student + '/', dataToSend, function (data) {
            if($('.error-msg', $(data)).length) {
                $('.super-modal').html(data);
            } else {
                $('#data-students').html(data);
                $('.shadow').trigger('click');
            }
        });
    });

    $('html').on('click', '.shadow', function () {
        $(this).remove();
        $('.super-modal').hide();
        $('.modal-delete').hide()
    });
})
