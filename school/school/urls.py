"""django_school URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
import debug_toolbar
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from core import views
from api import views as api_views
from django.contrib.auth.views import LoginView, LogoutView
from django.views.generic.base import TemplateView
from rest_framework.routers import SimpleRouter
from rest_framework.authtoken.views import obtain_auth_token

# Creating simple router and api path
router = SimpleRouter()
router.register('teachers', api_views.TeacherView)
router.register('students', api_views.StudentView)
router.register('groups', api_views.GroupView)


urlpatterns = [
    # django urls
    path('admin/', admin.site.urls),
    # main urls
    path('', views.GroupView.as_view(), name="index"),
    path('add/group/', views.AddGroupView.as_view(), name="add_group"),
    path('update/group/<int:pk>/', views.UpdateGroupView.as_view(), name="update_group"),
    path('login/', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('teachers/', views.TeacherView.as_view(), name="teachers"),
    path('students/', views.StudentView.as_view(), name="students"),
    path('students/add/', views.AddStudentView.as_view(), name='add_student'),
    path('students/update/<int:pk>/', views.UpdateStudentView.as_view(), name="update_student"),
    path('students/delete/<int:pk>/', views.DeleteStudentView.as_view(), name="delete_student"),
    path('contact_us/', views.ContactUsView.as_view(), name='contact_us'),
    path('contact_us/done/', TemplateView.as_view(template_name="contact_us_done.html"), name="contact_us_done"),
    path('registration/', views.RegistrationView.as_view()),
    # download and test urls:
    path('download/students/', views.ExportStudentsCSV.as_view(), name='download_students'),
    path('test/', views.TestView.as_view(), name="test"),
    # Api urls:
    path('api-auth/', include('rest_framework.urls')),
    path('api/', include(router.urls)),
    path('api-token-auth/', obtain_auth_token, name='api-token-auth'),
    # debug + static
    path('__debug__/', include(debug_toolbar.urls)),
] + static(
    settings.STATIC_URL, document_root=settings.STATIC_ROOT
) + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
)