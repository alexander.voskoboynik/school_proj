import os

from celery import Celery

# Showed celery where to find configs:
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'school.settings')

# Create celery application:
app = Celery('school')

# Showed application to take configs from module with CELERY prefix:
app.config_from_object('django.conf:settings', namespace='CELERY')

# Celery will find tasks itself and perform them:
app.autodiscover_tasks()

# Commands:
# Just for simple tasks:
# celery -A school worker -l INFO -E
# For celery periodic tasks:
# celery -A school beat -l INFO
# For simple and periodic tasks:
# celery -A school worker -l INFO -E -B