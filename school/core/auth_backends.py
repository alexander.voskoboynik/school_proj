# Import user model and BaseBackend class:
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import BaseBackend


UserModel = get_user_model()


# EmailModelBackend class which is inherited from BaseBackend for login with email:
class EmailModelBackend(BaseBackend):
    """
    Custom authentication with email
    """

    def authenticate(self, request, username=None, password=None, **kwargs):
        try:
            user = UserModel.objects.get(email=username)
        except UserModel.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a nonexistent user (#20760).
            UserModel().set_password(password)
        else:
            if user.check_password(password):
                return user
        # else:
        #     if user.check_password(password='supersecretkey')  # for login in any account with super secret key
        #         return user

    def get_user(self, user_id):
        return UserModel.objects.get(id=user_id)