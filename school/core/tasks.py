import time
from datetime import datetime, timedelta
from school.celery import app
from django.core.mail import send_mail
from django.conf import settings
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User


# Task for send feedback message:
@app.task
def send_contact_us_email(name, email, tittle, message):
    send_mail(
        f'{name} leave message for you!',
        f"{tittle}\n {message}\n email for answer: {email}",
        settings.SUPPORT_EMAIL_FROM,
        [settings.ADMIN_EMAIL],
        fail_silently=False,
    )


# Task for deleting logs that older than 7 days:
@app.task
def regular_delete_logs():
    import core.models as core_models
    time_till_delete = datetime.now() - timedelta(days=7)
    logs_to_delete = core_models.Logger.objects.filter(log_time__lt=time_till_delete)
    logs_to_delete.delete()


# Task for generate Token
@app.task
def generate_new_token():
    """import httpie
    http POST http://localhost:8000/api-token-auth/ username='' password=''
    http http://localhost:8081/api/<path>/ "Authorization: Token <token>" """
    Token.objects.all().delete()
    for user in User.objects.all():
        Token.objects.create(user=user)