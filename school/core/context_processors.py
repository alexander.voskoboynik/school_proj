from core.forms import StudentForm


def student_form(request):
    return {
        'student_form': StudentForm
    }
