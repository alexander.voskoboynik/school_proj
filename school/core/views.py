# standard imports
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.shortcuts import render, redirect
from django.urls.base import reverse_lazy
from django.db.models.aggregates import Count, Avg, Max, Min
from django.views.generic.base import View, TemplateView
from django.views.generic import CreateView, ListView, FormView, UpdateView, \
    DeleteView  # Views for useful work with Forms
from django.http import HttpResponse
import csv
from datetime import timedelta
# core app imports
from core.models import Group, Teacher, Student, MyUser
from core.forms import GroupForm, StudentForm, ContactUsForm, RegistrationForm


# Create Group view:
class GroupView(TemplateView):
    template_name = 'group.html'

    def get_context_data(self, **kwargs):
        context = super(GroupView, self).get_context_data(**kwargs)
        groups = Group.objects.annotate(stq=Count('student'), stavg=Avg('student__age'),
                                        stmax=Max('student__age'),
                                        stmin=Min('student__age')).prefetch_related('group_lead')
        context['groups'] = groups
        return context


# Create Test view for :
class TestView(TemplateView):
    template_name = 'test.html'

    def get_context_data(self, **kwargs):
        context = super(TestView, self).get_context_data(**kwargs)
        context['lst'] = [0, 34, 98, 45, 39, 86, 44, 30, 45, 4]
        return context


# Create Teacher view:
# @method_decorator(cache_page(60 * 15), name='dispatch')
class TeacherView(TemplateView):
    template_name = 'teachers.html'

    def get_context_data(self, **kwargs):
        context = super(TeacherView, self).get_context_data(**kwargs)
        context['teachers'] = Teacher.objects.all()
        return context


# Create Student view:
class StudentView(ListView):
    template_name = 'students.html'
    model = Student
    queryset = Student.objects.all().prefetch_related("course")
    context_object_name = 'students'
    paginate_by = 5

    # def get_context_data(self, **kwargs):
    #     context = super(StudentView, self).get_context_data(**kwargs)
    #     context['students'] = Student.objects.all().prefetch_related("course")
    #     return context

# TemplateViews for add new Group and Student:
# AddGroupView for Group model via TemplateView(and FormView):
# class AddGroupView(FormView):
#     template_name = 'add_update_group.html'
#     form_class = GroupForm
#     success_url = '/'
#
#     def form_valid(self, form):
#         form.save()
#         return super(AddGroupView, self).form_valid(form)


# @method_decorator(cache_page(60 * 60 * 8), name='dispatch')
class AddGroupView(TemplateView):
    template_name = 'add_update_group.html'

    def get_context_data(self, **kwargs):
        context = super(AddGroupView, self).get_context_data(**kwargs)
        context['form'] = GroupForm()
        return context

    def post(self, request):
        form = GroupForm(data=request.POST, files=request.FILES)  # files=request.FILES for images(probably in Future)
        if form.is_valid():
            form.save()
            return redirect('/')
        context = self.get_context_data()
        context['form'] = form
        return self.render_to_response(context)


# AddStudentView for Student Model via CreateView(and TemplateView):
# class AddStudentView(TemplateView):
#     template_name = 'student_form_add.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(AddStudentView, self).get_context_data(**kwargs)
#         context['form'] = StudentForm()
#         return context
#
#     def post(self, request):
#         form = StudentForm(data=request.POST, files=request.FILES)
#         if form.is_valid():
#             form.save()
#             return redirect('/')
#         context = self.get_context_data()
#         context['form'] = form
#         return self.render_to_response(context)


class AddStudentView(CreateView):
    template_name = 'student_add_update.html'
    success_url = reverse_lazy('index')
    model = Student
    form_class = StudentForm

    def get_template_names(self):
        if self.request.is_ajax:
            return ['includes/student_form_add.html']
        return [self.template_name]

    def form_valid(self, form):
        if self.request.is_ajax:
            form.save()
            context = ({'students': Student.objects.all().prefetch_related("course")})
            return render(template_name='includes/students-data.html', context=context, request=self.request)
        else:
            super(AddStudentView, self).form_valid(form)


# TemplateViews for update existing Group and Student:
# UpdateGroupView for Group model via FormView:
class UpdateGroupView(FormView):
    template_name = 'add_update_group.html'
    form_class = GroupForm
    success_url = reverse_lazy('index')

    def get_initial(self):
        name = Group.objects.get(id=self.kwargs['pk']).course
        return {'course': name}

    def form_valid(self, form):
        # group = Group.objects.get(id=self.kwargs['pk'])
        # group.course = form.cleaned_data['course']
        # group.group_lead.set(form.cleaned_data['group_lead'])
        pk = self.request.path.split('/')[-2]
        form.update_save(pk)
        return super(UpdateGroupView, self).form_valid(form)


# UpdateStudentView for Student Model via UpdateView:
class UpdateStudentView(UpdateView):
    template_name = 'student_add_update.html'
    success_url = reverse_lazy('index')
    model = Student
    form_class = StudentForm

    def get_template_names(self):
        if self.request.is_ajax:
            return ['includes/student_form_edit.html']
        return [self.template_name]

    def form_valid(self, form):
        if self.request.is_ajax:
            form.save()
            context = ({'students': Student.objects.all().prefetch_related("course")})
            return render(template_name='includes/students-data.html', context=context, request=self.request)
        else:
            super(UpdateStudentView, self).form_valid(form)


class DeleteStudentView(DeleteView):
    template_name = 'student_delete_form.html'
    success_url = reverse_lazy('index')
    model = Student

    def delete(self, request, *args, **kwargs):
        context = ({'students': Student.objects.all().prefetch_related("course")})
        if self.request.is_ajax:
            self.object = self.get_object()
            self.object.delete()
            return render(template_name='includes/students-data.html', context=context, request=self.request)
        else:
            super(DeleteStudentView, self).delete(*args, **kwargs)


# ContactUsView for user feedback:
# @method_decorator(cache_page(timedelta(days=365 * 99).seconds), name='dispatch')
class ContactUsView(FormView):
    template_name = "contact_us.html"
    form_class = ContactUsForm
    success_url = reverse_lazy('contact_us_done')

    def form_valid(self, form):
        form.save()
        return super(ContactUsView, self).form_valid(form)


# Students to CSV:
class ExportStudentsCSV(View):

    def get(self, request):
        queryset = Student.objects.all().prefetch_related('course')
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="students.csv"'
        writer = csv.writer(response)
        writer.writerow(['ID', 'Name', 'Email', 'Age', 'Course'])
        for student in queryset:
            writer.writerow([student.id, student.name, student.email, str(student.age),
                             ', '.join([student.course for student in student.course.all()])])
        return response


class RegistrationView(CreateView):
    template_name = 'registration.html'
    success_url = '/'
    model = MyUser
    form_class = RegistrationForm