from django.core.management.base import BaseCommand
import requests
import lxml.html as lh
from core.models import ExchangeCurrency


class Command(BaseCommand):

    def handle(self, *args, **options):
        response = requests.get("https://kurs.com.ua/gorod/2249-harkov")
        if response.status_code == 200:
            tree = lh.fromstring(response.content)
            currency_rate = []
            # print(tree.xpath('/html/body/div[3]/div[3]/div/div[1]/div[1]/div[1]/div/table/tbody')[0])
            tree_x_path = tree.xpath('//*[@id="main_table"]/tbody')[0]
            # print(tree_x_path.text_content())
            for tr in tree_x_path[0:3]:
                currency_rate.append(tr.text_content().split())
                # print(currency_rate)
            if '-' or '+' in currency_rate[2][2]:
                ExchangeCurrency.objects.create(
                    source='kurs.com.ua',
                    dollar=f"USD: {currency_rate[0][1]}₴ / {currency_rate[0][3]}₴\n",
                    euro=f"EUR: {currency_rate[1][1]}₴ / {currency_rate[1][3]}₴\n",
                    ruble=f"RUB: {currency_rate[2][1]}₴ / {currency_rate[2][3]}₴",
                )
            if '-' or '+' not in currency_rate[2][2]:
                ExchangeCurrency.objects.create(
                    source='kurs.com.ua',
                    dollar=f"USD: {currency_rate[0][1]}₴ / {currency_rate[0][3]}₴\n",
                    euro=f"EUR: {currency_rate[1][1]}₴ / {currency_rate[1][3]}₴\n",
                    ruble=f"RUB: {currency_rate[2][1]}₴ / {currency_rate[2][2]}₴",
                )