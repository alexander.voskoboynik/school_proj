from django.core.management.base import BaseCommand
import requests
import lxml.html as lh
from core.models import ExchangeCurrency


class Command(BaseCommand):

    def handle(self, *args, **options):
        response = requests.get("https://finance.i.ua/")
        if response.status_code == 200:
            tree = lh.fromstring(response.content)
            currency_rate = []
            # print(tree.xpath('/html/body/div[3]/div[3]/div/div[1]/div[1]/div[1]/div/table/tbody')[0])
            tree_x_path = tree.xpath('/html/body/div[3]/div[3]/div/div[1]/div[1]/div[1]/div/table/tbody')[0]
            # print(tree_x_path.text_content())
            for tr in tree_x_path:
                # currency_rate.append(tr.text_content())
                # print(currency_rate)
                for field in tr:
                    currency_rate.append(field.text_content()[0:7])
                    # print(currency_rate)
            ExchangeCurrency.objects.create(
                source='Fiance.i.ua',
                dollar=f"USD: {currency_rate[1]}₴ / {currency_rate[2]}₴\n",
                euro=f"EUR: {currency_rate[5]}₴ / {currency_rate[6]}₴\n",
                ruble=f"RUB: {currency_rate[9]}₴ / {currency_rate[10]}₴",
            )