from django.db import models  # for making Class(tables) in DB
from django.db.models.aggregates import Count, Avg, Max, Min
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.auth import get_user_model
from core.tasks import send_contact_us_email


# Class for AbstractUser:
class MyUser(AbstractUser):
    phone = models.CharField(max_length=15, blank=True, null=True)
    age = models.IntegerField(blank=True, null=True)


# GroupManager for aggregating Group Model with Student Model:
# class GroupManager(models.Manager):
#
#     def group_student_age_count(self):
#         return self.get_queryset().annotate(stq=Count('student'), stavg=Avg('student__age'),
#         stmax=Max('student__age'), stmin=Min('student__age'))


# Create Group Model:
class Group(models.Model):
    course = models.CharField(max_length=255)
    group_lead = models.ManyToManyField('core.Teacher', blank=True)

    def __str__(self):
        return self.course

    # objects = GroupManager()


# Create Teacher Group Model:
class Teacher(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField(max_length=40)

    class Meta:
        unique_together = ('name', 'email')

    def __str__(self):
        return self.name


# Create Student model:
class Student(models.Model):

    GENDER_MALE = 1
    GENDER_FEMALE = 2
    GENDER_ALIEN = 3
    GENDER_FICTIONAL_CHARACTER = 4

    GENDER_CHOICES = (
        (GENDER_MALE, 'Male'),
        (GENDER_FEMALE, "Female"),
        (GENDER_ALIEN, "Alien"),
        (GENDER_FICTIONAL_CHARACTER, "Fictional character"),
    )

    name = models.CharField(max_length=255, null=False, blank=False)
    age = models.IntegerField()
    gender = models.PositiveSmallIntegerField(choices=GENDER_CHOICES, blank=True)
    course = models.ManyToManyField(Group, blank=True)
    email = models.EmailField(max_length=40, blank=True, null=True)

    class Meta:
        unique_together = ('name', 'email')

    def __str__(self):
        return self.name


# ContactUs Model for user feedback:
class ContactUs(models.Model):

    name = models.CharField(max_length=255)
    email = models.EmailField()
    tittle = models.CharField(max_length=255)
    message = models.TextField()

    def __str__(self):
        return f"{self.name} - {self.tittle}"

    def save(self, *args, **kwargs):
        super(ContactUs, self).save(*args, **kwargs)
        send_contact_us_email.delay(self.name, self.email, self.tittle, self.message)


class Logger(models.Model):
    path = models.CharField(max_length=255)
    method = models.CharField(max_length=255)
    log_time = models.DateTimeField(default=None)
    execution_time = models.FloatField()

    def __str__(self):
        return f'{self.path} -> {self.method}, {self.log_time}'


class ExchangeCurrency(models.Model):
    source = models.CharField(max_length=255)
    dollar = models.CharField(max_length=25, null=True)
    euro = models.CharField(max_length=25, null=True)
    ruble = models.CharField(max_length=25, null=True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'Source: {self.source}, Date: {self.date}'
