from django.test import TestCase
# import objects we need to test:
from core.views import (GroupView, AddGroupView, UpdateGroupView, TeacherView, AddStudentView,
                        UpdateStudentView, TestView)
from core.models import Student, Group, Teacher
from core.templatetags.custom_tags import even_numbers_from_array, words_quantity_in_string


# -> Create class for testing urls:
class SchoolTestCases(TestCase):

    """Using setUp for creating example of GroupModel, StudentModel, TeacherModel"""
    def setUp(self):
        super(SchoolTestCases, self).setUp()
        self.teacher = Teacher.objects.create(name="Advanced Teacher", email="python.teacher@admin.com")
        self.group = Group.objects.create(course='TestRookie')
        self.student = Student.objects.create(name="RookieStudent", age='34', gender=1,
                                              email="rookie.student@admin.com")

    """
    Methods for test HTTP response for available urls
    """
    def test_index_url(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200, 'Bad response status code')

    def test_teachers_url(self):
        response = self.client.get('/teachers/')
        self.assertEqual(response.status_code, 200)

    def test_add_group_url(self):
        response = self.client.get('/add/group/')
        self.assertEqual(response.status_code, 200)

    def test_add_student_url(self):
        response = self.client.get('/add/student/')
        self.assertEqual(response.status_code, 200)

    def test_update_group_url(self):
        response = self.client.get('/update/group/1/')
        self.assertEqual(response.status_code, 200)

    def test_update_student_url(self):
        response = self.client.get('/update/student/1/')
        self.assertEqual(response.status_code, 200)

    def test_test_url(self):
        response = self.client.get('/test/')
        self.assertEqual(response.status_code, 200, '404')

    """
       Methods for check Views.Template names
    """

    def test_group_template_name(self):
        """attribute in this template should be group.html"""
        view = GroupView
        self.assertEqual(view.template_name, 'group.html')

    def test_teachers_template_name(self):
        """attribute in this template should be teachers.html"""
        view = TeacherView
        self.assertEqual(view.template_name, 'teachers.html')

    def test_group_add_update_template_name(self):
        """attribute in this template should be add_update_group.html"""
        views = [AddGroupView, UpdateGroupView]
        for view in views:
            self.assertEqual(view.template_name, 'add_update_group.html')

    def test_student_template_name(self):
        """attribute in this template should be student_form_add.html"""
        views = [AddStudentView, UpdateStudentView]
        for view in views:
            self.assertEqual(view.template_name, 'student_form_add.html')

    """
    Test for custom filters
    """
    def test_even_numbers_from_array(self):
        expected_list = [4, 8, 12]
        past_list = [1, 5, 4, 43, 8, 131, 12]
        call_func = even_numbers_from_array(past_list)
        self. assertEqual(expected_list, call_func, "not working")

    def test_words_quantity_in_string(self):
        expected_words = 5
        sentence = words_quantity_in_string('Hello, my - name is ? Vova.')
        self.assertEqual(expected_words, sentence)

    """
    Test context data for '/' and /teachers/
    """
    def test_group_view_context(self):
        group = Group.objects.group_student_age_count().prefetch_related('group_lead')
        response = self.client.get('/')
        self.assertQuerysetEqual(response.context['groups'], map(repr, group))
        # self.assertQuerysetEqual(response.context['groups'], group, transform=lambda x: x) : almost same thing
        # but does the opposite

    def test_teachers_view_context(self):
        group_lead = Teacher.objects.all()
        response = self.client.get('/teachers/')
        self.assertQuerysetEqual(response.context['teachers'], map(repr, group_lead))